================
SLEPc for Python
================

Notice
------

Since February 2021, slepc4py sources have been imported into the main SLEPc repository https://gitlab.com/slepc/slepc - more precisely, you can find slepc4py files under `src/binding/slepc4py`: https://gitlab.com/slepc/slepc/-/tree/main/src/binding/slepc4py

Issues and merge requests should be opened via the SLEPc repository.


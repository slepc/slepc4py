
SLEPc for Python
================

**WARNING**: This is no longer the official repository of slepc4py.

The new repository is hosted at [gitlab.com](http://gitlab.com/slepc/slepc4py).
This clone contains commits only up to Dec 10, 2019.

To update your local repository to use the new location, you need to do

    $ git remote set-url origin git@gitlab.com:slepc/slepc4py.git 

or

    $ git remote set-url origin https://gitlab.com/slepc/slepc4py.git

